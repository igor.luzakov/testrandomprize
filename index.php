<?php

include_once __DIR__ .'/core/Connect.php';
include_once __DIR__ . '/vendor/autoload.php';
include_once __DIR__ .'/app/AppApi.php';

use App\AppApi;
session_start();

try {
    $api = new AppApi();
    if(empty($_SESSION['user'])){
        echo $api->notUser();
        return;
    }
    echo $api->run();

} catch (Exception $e) {
    echo $api->response($e->getMessage(),$api::STATUS_500);
}

/*
 {
    "controller": "User",
    "method": "users",
    "params": {
        "from": 1,
        "to": "1"
    }
}

{
    "controller": "User",
    "method": "login",
    "params": {
        "login": "user/root",
        "password": "1234"
    }
}

{
    "controller": "User",
    "method": "unlogin"
}

{
    "controller": "Prize",
    "method": "savePrizes",
    "params": {
        "code": 3,
        "object": {
                    "id": "1",
                    "name": "Приз1"
                    },
        "prize_id": 3,
        "user_id": 1,
        "money": 0,
        "bonus": 0,
        "active":true
    }
}

 {
    "controller": "Prize",
    "method": "randomPrize"

}
 */