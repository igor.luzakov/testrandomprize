<?php
namespace App;
include_once __DIR__ .'/../core/Api.php';
use Core\Api;

class AppApi extends Api
{


    public function notUser(){
        if($this->action === 'login'){
            return $this->run();
        }
        return $this->response("User not",self::STATUS_404);
    }
}