<?php
namespace Controller;

use App\AppApi;
use Core\Connect;

class UserController extends AppApi
{
    public function users(){
        $params = $this->request['params'];
        $result = $this->model->getUsrers($params);
        return $this->response($result,self::STATUS_200);
    }

    public function login(){
        $params = $this->request['params'];
        if($params['password']=='1234'){
            $user = Connect::selectOne("SELECT * FROM user WHERE name='{$params['login']}'");
            if($user['active']){
                $_SESSION['user'] =  $user;
                return $this->response("Ok",self::STATUS_200);
            }

        }

        return $this->response("User not",self::STATUS_404);
    }

    public function unlogin(){
        unset($_SESSION['user']);
        return $this->response("Ok",self::STATUS_200);
    }
}