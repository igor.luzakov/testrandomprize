<?php
namespace Controller;

use App\AppApi;
use Core\Connect;

class PrizeController extends AppApi
{
    public function randomPrize(){
        $params = $this->request['params'];
        $prizes = $this->model->getPrizes($params);
        $rand_keys = array_rand($prizes, 1);
        $prize = $prizes[$rand_keys];
        $prizeMoney = 0;
        $prizeBonus = 0;
        $prizeObject = [];
        switch ($prize['code']){
            case 1:
                $prizeMoney = mt_rand(10,100);
                break;
            case 2:
                $prizeBonus = mt_rand(100,1000);
                break;
            case 3:
                $res = $this->model->getPrizeObject($params);
                $prizeObject = $res[array_rand($res, 1)];
                break;
        }
        $result = [
            'prize_id' => $prize['id'],
            'code' => $prize['code'],
            'user_id'  => $_SESSION['user']['id'],
            'money' => $prizeMoney,
            'bonus' => $prizeBonus,
            'object' => $prizeObject,
        ];
        return $this->response($result,self::STATUS_200);
    }

    public function savePrizes(){
        if($this->method == 'PUT'){
            $params = $this->request['params'];
            $res = $this->model->savePrize($params);
            return $this->response([$res],$res ? self::STATUS_200:self::STATUS_404);
        }

        return $this->response("Method {$this->method}",self::STATUS_404);
    }



}