# testRandomPrize

console:

![](help1.png)

![](help2.png)

API:

POST:
Войти под пользователем root или user ("login": "root")
```json
{
"controller": "User",
"method": "login",
"params": {
"login": "root",
"password": "1234"
}
}
```
POST:
Разлогиниться
```json
{
"controller": "User",
"method": "unlogin"
}
```
POST:
Просмотр пользователей (limit from,to)
```json
{
  "controller": "User",
  "method": "users",
  "params": {
    "from": 1,
    "to": "1"
  }
}
```
POST:
Random призов
```json
{
"controller": "Prize",
"method": "randomPrize"

}
```

PUT:
Сохранить приз пользователю
```json
{
"controller": "Prize",
"method": "savePrizes",
"params": {
    "code": 3,
    "object": {
        "id": "1",
        "name": "Приз1"
        },
    "prize_id": 3,
    "user_id": 1,
    "money": 0,
    "bonus": 0,
    "active":true
    }
}
```




