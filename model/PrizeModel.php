<?php
namespace Model;

use Core\Connect;

class PrizeModel
{
    public function getPrizes($params){
        return Connect::select("SELECT * FROM prizes",$params);
    }

    public function getPrizeObject($params){
        return Connect::select("SELECT id,name FROM prizes_object WHERE quantity>0",$params);
    }

    public function savePrize($params){
        $list = [];
        $id = 0;
        if($params['code'] === 3){
            $id = $params['object']['id'];
            $binds[] = [':id'=>$id];
            $prizesObject = Connect::selectOne("SELECT * FROM prizes_object WHERE id=:id",[':id'=>$id]);
            if($prizesObject['quantity'] > 0){
                $quantity = $prizesObject['quantity'] - 1;
                $list[] = "UPDATE prizes_object SET quantity={$quantity} WHERE id=:id;";
            }else{
                return false;
            }
        }
        $binds[] = [
            ':prize_id'=>$params['prize_id'],
            ':user_id'=>$params['user_id'],
            ':object_id'=>$id,
            ':bonus' => $params['bonus'],
            ':money' => $params['money'],
            ':active'=>$params['active']
        ];
        $list[] = "INSERT INTO user_prizes( prize_id, user_id,object_id, bonus,money, active) 
                                    VALUES (:prize_id,:user_id,:object_id,:bonus,:money,:active);";

        return Connect::insertList($list,$binds);
    }


}