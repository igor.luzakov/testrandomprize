<?php

namespace Core;


abstract class Api
{
    public const STATUS_200 = 200;
    public const STATUS_404 = 404;
    public const STATUS_405 = 405;
    public const STATUS_500 = 500;



    protected $method = ''; //GET|POST|PUT|DELETE
    protected $request = [];
    protected $model;
    protected $action = ''; //Название метод для выполнения
    protected $controller;


    public function __construct($model = null) {
        $this->model = $model;
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        $this->initRequest();
        $this->action = $this->request['method'];



    }

    public function run(){

        $this->controller = $this->getController();
        //Если метод(действие) определен в дочернем классе API
        if (method_exists($this->controller, $this->action)) {
            return $this->controller->{$this->action}();
        } else {
            return $this->response('Invalid Method ' . $this->status,self::STATUS_405);

        }
    }

     private function initRequest(){
        switch ($this->method) {
            case 'GET':
                $this->request = $_GET;
                break;
            case 'POST':
            case 'PUT':
            case 'DELETE':
                $this->request = (array)json_decode(file_get_contents('php://input'), true);
                break;
            default:
                break;
        }
    }



    public function response($data,$status) {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        return json_encode($data);
    }

    private function requestStatus($code) {
        $status = array(
            self::STATUS_200 => 'OK',
            self::STATUS_404 => 'Not Found',
            self::STATUS_405 => 'Method Not Allowed',
            self::STATUS_500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[self::STATUS_500];
    }

    protected function getController()
    {
        $class = "controller\\".$this->request['controller']."Controller";
        include_once $class . '.php';
        $model = "model\\".$this->request['controller']."Model";
        include_once $model . '.php';

        // Проверяем необходимость подключения указанного класса
        if (!class_exists($class, false)) {
            trigger_error("Unable to load class: $class", E_USER_WARNING);
        }


        if (class_exists($class) && class_exists($model)) {
            $myclass = new $class(new $model());
        }
        return $myclass;

    }

}