<?php
namespace Core;


class Connect{

    protected static $connect;

    protected function __construct(){

	}

    /**
     * @return \Core\PDO|void
     */
	public static function initConnect()
    {
        if(empty(self::$connect)){
            self::$connect = self::connect();
        }
        return self::$connect;
    }

	private static function connect(){
        try{
            $dsn = 'mysql:dbname=project;host=127.0.0.1';
            $user = 'root';
            $password = '';
            return new \PDO($dsn, $user, $password);
        }catch(PDOException $error){
            echo $error->getMessage();
        }

}
	
	public static function select($sql,$params = []){
        $from = 0;
        $to = 10;
        if(isset($params['from'])){
            $from = $params['from'];
        }
        if(isset($params['to']) && $params['to'] <= 1000){
            $to = $params['to'];
        }
        $sql = $sql." LIMIT $from, $to" ;
        $db = self::initConnect();
        $stm = $db->prepare($sql);
        $stm->execute();
        return $stm->fetchAll(\PDO::FETCH_ASSOC);
    }
    public static function selectOne($sql,$binds = []){
        $db = self::initConnect();
        $stm = $db->prepare($sql);
        $stm->execute($binds);
        return $stm->fetch(\PDO::FETCH_ASSOC);
    }

    public static function insert($sql,$binds = []){
        $db = self::initConnect();
        $stm = $db->prepare($sql);


        $stm->execute($binds);
        return $db->lastInsertId();
    }

    public static function insertList(array $sqlList,$binds = []){
        $db = self::initConnect();
        try {
            $db->beginTransaction();
            foreach ($sqlList as $key => $sql){
                $stm = $db->prepare($sql);
                $result = $stm->execute($binds[$key]);
                if($result === false){
                    $db->rollBack();
                    return false;
                }
            }
            $db->commit();
            return true;
        }catch (\PDOException $e){
            $db->rollBack();
        }

        return false;
    }


}


