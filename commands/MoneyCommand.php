<?php

namespace Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MoneyCommand extends Command
{

    protected function configure()
    {
        $this->setName('money:spend')
            ->setDescription('Отправить перевод')
            ->setHelp('Отправить перевод')
            ->addArgument('money', InputArgument::OPTIONAL, 'Деньги:',0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $beginNameMigration = $input->getArgument('money');
        $output->writeln("========== SPEND money =========");
        $output->writeln("================= MONEY $beginNameMigration $================");
        $output->writeln("=========== END SPEND money =========");

        return Command::SUCCESS;
    }
}